import myparamiko
import getpass
import time

from jinja2 import Template

device = {
    "hostname": 'sbx-nxos-mgmt.cisco.com',
    "port": 8181,
    "username": 'admin',
    "password": 'Admin_1234!'
}

data = {
    "loopbacks": [
        {
            'loopbackid': 11
        },
        {
            'loopbackid': 12
        },
        {
            'loopbackid': 13
        },
        {
            'loopbackid': 14
        },
        {
            'loopbackid': 15
        }
    ]
}

try:    
    myparamiko.log('connecting')    
    ssh_client = myparamiko.connect(**device)
    remote_connection = myparamiko.get_shell(ssh_client)    
    myparamiko.log('connection ... success')  
    myparamiko.log('sending sh ip int br')    
    output = myparamiko.send_command(remote_connection, 'sh ip int br')
    # print(output)
    myparamiko.log('creating interfaces')

    with open('new_interface2.j2') as configs:
        interface_template = Template(configs.read())
        interface = interface_template.render(data)
        print(interface)
        for cmd in interface.split('\n'):
            sleep = 0 if cmd != "end" else 3
            myparamiko.send_command(remote_connection, cmd, sleep)
    
    myparamiko.log('sending sh ip int br')
    
    output = myparamiko.send_command(remote_connection, 'sh ip int br')
    print(output)

finally:
    myparamiko.log('closing connectin')
    ssh_client.close()