from napalm import get_network_driver
import pprint 

config = {
    "hostname": 'ios-xe-mgmt.cisco.com',
    "username": 'developer',
    "password": 'C1sco12345'
}

driver = get_network_driver('ios')

optional_arguments={
    "port": 8181
}

with driver(**config,
            optional_args=optional_arguments) as device:
    interfaces = device.get_interfaces()
    pprint.pprint(interfaces)