from netmiko import Netmiko
from jinja2 import Template
devices = [
    {
        "host": 'sbx-nxos-mgmt.cisco.com',
        "port": 8181,
        "username": 'admin',
        "password": 'Admin_1234!',
        "device_type": "cisco_nxos",
        "session_log": "sbx-nxos-mgmt.cisco.com.log"
    },
    {
        "host": 'sbx-iosxr-mgmt.cisco.com',
        "port": 8181,
        "username": 'admin',
        "password": 'C1sco12345',
        "device_type": "cisco_xr",
        "session_log": "sbx-iosxr-mgmt.cisco.com.log"
    },
    {
        "host": 'ios-xe-mgmt.cisco.com',
        "port": 8181,
        "username": 'developer',
        "password": 'C1sco12345',
        "device_type": "cisco_ios",
        "session_log": "ios-xe-mgmt.cisco.com.log"
    }
]

data = {
    "loopbacks": [
        {
            'loopbackid': 11
        },
        {
            'loopbackid': 12
        },
        {
            'loopbackid': 13
        },
        {
            'loopbackid': 14
        },
        {
            'loopbackid': 15
        }
    ]
}

for device in devices:
    connection = Netmiko(**device)
    try:
        with open('new_interface_3.j2') as configs:
            interface_template = Template(configs.read())
            interface = interface_template.render(data)
            print(interface)
            cmd = interface.split('\n') # lista
            output = connection.send_config_set(cmd)
            print(output)
    finally:
        connection.disconnect()