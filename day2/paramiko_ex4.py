import myparamiko
import getpass
import time

device = {
    "hostname": 'sbx-nxos-mgmt.cisco.com',
    "port": 8181,
    "username": 'admin',
    "password": 'Admin_1234!'
}

try:
    
    myparamiko.log('connecting')
    
    ssh_client = myparamiko.connect(**device)
    remote_connection = myparamiko.get_shell(ssh_client)
    
    myparamiko.log('connection ... success')
    
    
    myparamiko.log('sending sh ip int br')
    
    output = myparamiko.send_command(remote_connection, 'sh ip int br')
    print(output)
    
    myparamiko.log('creating interfaces')
    myparamiko.send_command(remote_connection, 'conf t')
    myparamiko.send_command(remote_connection, 'int loopback 11')
    myparamiko.send_command(remote_connection, 'ip address 1.1.2.11 255.255.255.255')
    myparamiko.send_command(remote_connection, 'int loopback 12')
    myparamiko.send_command(remote_connection, 'ip address 1.1.2.12 255.255.255.255')
    myparamiko.send_command(remote_connection, 'end')
    
    myparamiko.log('sending sh ip int br')
    
    output = myparamiko.send_command(remote_connection, 'sh ip int br')
    print(output)

finally:
    
    myparamiko.log('closing connectin')
    
    ssh_client.close()