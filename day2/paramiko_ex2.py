import paramiko
import getpass
import time

ssh_client = paramiko.SSHClient()

ssh_client.set_missing_host_key_policy(paramiko.AutoAddPolicy())

device = {
    "hostname": 'sbx-nxos-mgmt.cisco.com',
    "port": 8181,
    "username": 'admin',
    "password": getpass.getpass('password> '),
    "look_for_keys": False, 
    "allow_agent": False
}

try:
    ssh_client.connect(**device)
    remote_connection = ssh_client.invoke_shell()

    remote_connection.send('sh ip int br\n')

    remote_connection.send('enable\n')
    remote_connection.send('cisco\n')
    remote_connection.send('conf t\n')
    remote_connection.send('int loopback 0\n')
    remote_connection.send('ip address 1.1.1.1 255.255.255.255\n')
    remote_connection.send('int loopback 1\n')
    remote_connection.send('ip address 2.2.2.2 255.255.255.255\n')
    remote_connection.send('end\n')

    time.sleep(3)

    remote_connection.send('sh ip int br\n')

    output = remote_connection.recv(4096)
    print(output.decode())

    # stdin, stdout, stderr = ssh_client.exec_command('sh ip int br')
    # output = stdout.read().decode()
    # print(output)
finally:
    ssh_client.close()