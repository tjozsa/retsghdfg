from netmiko import Netmiko

devices = [
    {
        "host": 'sbx-nxos-mgmt.cisco.com',
        "port": 8181,
        "username": 'admin',
        "password": 'Admin_1234!',
        "device_type": "cisco_nxos",
        "session_log": "sbx-nxos-mgmt.cisco.com.log"
    },
    {
        "host": 'sbx-iosxr-mgmt.cisco.com',
        "port": 8181,
        "username": 'admin',
        "password": 'C1sco12345',
        "device_type": "cisco_xr",
        "session_log": "sbx-iosxr-mgmt.cisco.com.log"
    },
    {
        "host": 'ios-xe-mgmt.cisco.com',
        "port": 8181,
        "username": 'developer',
        "password": 'C1sco12345',
        "device_type": "cisco_ios",
        "session_log": "ios-xe-mgmt.cisco.com.log"
    }
]

for device in devices:
    connection = Netmiko(**device)
    try:
        output = connection.send_command('sh ip int brief')
        print(output)
    finally:
        connection.disconnect()