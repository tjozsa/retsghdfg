from napalm import get_network_driver

config = {
    "hostname": 'ios-xe-mgmt.cisco.com',
    "username": 'developer',
    "password": 'C1sco12345'
}

driver = get_network_driver('ios')

optional_arguments={
    "port": 8181
}

with driver(**config,
            optional_args=optional_arguments) as device:
    config = device.get_config()
    with open('my_ios_config.txt', 'w') as f:
        f.write(config['running'])