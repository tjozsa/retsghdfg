import myparamiko
import getpass
import time

from jinja2 import Template

device = {
    "hostname": 'sbx-nxos-mgmt.cisco.com',
    "port": 8181,
    "username": 'admin',
    "password": 'Admin_1234!'
}

data = {
    "loopbacks": [
        {
            'loopbackid': 11
        },
        {
            'loopbackid': 12
        },
        {
            'loopbackid': 13
        }
    ]
}

# data['loopbacks'][0]['loopbackid'] ---> 11

try:
    
    myparamiko.log('connecting')
    
    ssh_client = myparamiko.connect(**device)
    remote_connection = myparamiko.get_shell(ssh_client)
    
    myparamiko.log('connection ... success')
    
    
    myparamiko.log('sending sh ip int br')
    
    output = myparamiko.send_command(remote_connection, 'sh ip int br')
    print(output)
    
    myparamiko.log('creating interfaces')

    with open('new_interface.j2') as configs:
        interface_template = Template(configs.read())
        for loopback in data['loopbacks']:
            interface = interface_template.render(loopback)
            # print(interface)
            for cmd in interface.split('\n'):
               myparamiko.send_command(remote_connection, cmd)
    
    myparamiko.log('sending sh ip int br')
    
    output = myparamiko.send_command(remote_connection, 'sh ip int br')
    print(output)

finally:
    myparamiko.log('closing connectin')
    ssh_client.close()