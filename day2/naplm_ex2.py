from napalm import get_network_driver

config = {
    "hostname": 'ios-xe-mgmt.cisco.com',
    "username": 'developer',
    "password": 'C1sco12345'
}

driver = get_network_driver('ios')

optional_arguments={
    "port": 8181
}

with driver(**config,
            optional_args=optional_arguments) as device:
    device.load_merge_candidate('my_ios_config_change.txt')
    config = device.get_config()
    diff = device.compare_config()
    print(diff)
    device.commit_config()
    config = device.get_config()
    print(config)