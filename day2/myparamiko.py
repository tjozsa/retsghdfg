import paramiko
import time

def connect(hostname, port, username, password):
    client = paramiko.SSHClient()
    client.set_missing_host_key_policy(paramiko.AutoAddPolicy())
    client.connect(hostname, port=port, username=username, password=password, look_for_keys=False, allow_agent=False)
    return client

def get_shell(client):
    connection = client.invoke_shell()
    return connection

def send_command(connection, command, sleep=2):
    """ sleep default ereteke 2 sec, lehet 1.6 vagy barmi
            ha 0 : nincs sleep, es nincs kimenet sem
    """
    connection.send(command + '\n')
    output = None
    if sleep > 0:
        time.sleep(sleep)
        output = connection.recv(4096).decode()
    return output

def log(msg):
    print('*'*50)
    print(msg)
    print('*'*50)